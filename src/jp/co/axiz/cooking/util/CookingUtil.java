package jp.co.axiz.cooking.util;

import java.util.ArrayList;

/**
 *
 * @author t.sugai
 *
 */
public class CookingUtil {

	//カレーの材料
	//このメソッドを呼び出すことでカレーの食材をString配列で取得できる
	private static String[] getCarry() {
		String[] ingredients = { "ルー", "タマネギ", "ジャガイモ", "ニンジン", "肉" };
		return ingredients;
	}

	//チャーハンの材料
	//このメソッドを呼び出すことでチャーハンの食材をString配列で取得できる
	private static String[] getCharhang() {
		String[] ingredients = { "ごはん", "卵", "しょうゆ", "ごま油", "ハム" };
		return ingredients;
	}

	//Test1,2,9
	//引数nが1→カレーの材料、nが2→チャーハンの材料、その他→プログラム終了
	//をArrayList<String>のlistに格納する
	public static ArrayList<String> getIngredients(int n){
		ArrayList<String> list = new ArrayList<String>();
		//入力が1、カレー
		if(n == 1){
			String[] ing = getCarry();
			for(int i = 0;i < ing.length;i++){
				list.add(ing[i]);
			}
		}
		//入力が2、チャーハン
		else if(n == 2){
			String[] ing = getCharhang();
			for(int i = 0;i < ing.length;i++){
				list.add(ing[i]);
			}
		}
		//入力がそれ以外、listのアドレスをnullに
		else{
			list = null;
		}
		return list;
	}

	//Test3,4
	//文字列の追加
	//追加する文字列が配列ならsplitで分割してから追加
	public static void setIngredients(ArrayList<String> list,String str){
		//文字列に,がない、そのまま追加する
		if(str.indexOf(",") == -1){
			list.add(str);
		}
		//文字列に,がある、文字列を分割してから追加する
		else{
			String[] strs = str.split(",");
			for(int i = 0;i < strs.length;i++){
				list.add(strs[i]);
			}
		}
	}

	//Test5,6,7,8
	//ArrayListから要素を削除（0〜すでに存在する要素：以外なら削除しない）
	public static ArrayList<String> reduceIngredients(ArrayList<String> list,int selectReduce) {
		//0〜要素数の範囲内の時のみ要素を削除する
		if(selectReduce > 0 && selectReduce < list.size()+1){
			list.remove(selectReduce -1);
			return list;
		}else{
			return list;
		}
	}
}