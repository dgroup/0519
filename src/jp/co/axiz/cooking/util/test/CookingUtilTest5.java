package jp.co.axiz.cooking.util.test;

import java.util.ArrayList;
import jp.co.axiz.cooking.util.CookingUtil;
import junit.framework.TestCase;

/**
 *
 * @author t.sugai
 *
 */
public class CookingUtilTest5 extends TestCase {
	public void testSetIngredients() {
		//『食材を減らす』選択時に使用するメソッドのテストです。
		//※食材番号…料理を選択した際に表示される食材リストの番号
		ArrayList<String> list = new ArrayList<String>();

		//今回はカレーを選択したとしてselcetNumberを1で初期化します。
		int selectNumber = 1;

		//削除したい食材番号。今回は1(ルー)とします。
		int selectReduce = 1;

		//材料名をArrayList型の戻り値で返すgetIngredientsを呼び出し、結果をlistに代入します。
		//今回のテストではカレーの材料です。
		list = CookingUtil.getIngredients(selectNumber);

		//上記のlistと、listから削除したい食材番号selectReduceを引数としてreduceIngredientsを呼び出します。
		//今回のテストにおけるreduceIngredientsメソッドの戻り値はlistインスタンスです。
		//listの中身はルーが削除された状態のカレーの材料です。
		//ただし削除された食材番号の部屋にnullが入っていてはいけません。
		list = CookingUtil.reduceIngredients(list, selectReduce);

		
		//処理結果と想定された処理結果を比較、確認します。
		//食材「ルー」がlistの0番目の部屋から削除され、1番目の部屋の食材「タマネギ」が繰り上がって代入されていれば成功です。
		assertFalse("ルーが削除されていない", list.get(0).equals("ルー"));
		assertEquals("削除結果がおかしい", "タマネギ", list.get(0));
	}
}
