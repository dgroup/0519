package jp.co.axiz.cooking.util.test;

import java.util.ArrayList;
import jp.co.axiz.cooking.util.CookingUtil;
import junit.framework.TestCase;

/**
 *
 * @author t.sugai
 *
 */
public class CookingUtilTest9 extends TestCase {
	//カレーかチャーハンを選択するメソッドのテストです。
	//1,2以外の数字を入力した場合は呼び出し元でエラー処理をするため、getIngredientsメソッドはnullを返します。
	public void testGetIngredients() {

		// 1，2以外の値を引数としてgetIngredientsメソッドを呼び出しlistに戻り値を代入します。
		ArrayList<String> list = CookingUtil.getIngredients(-1);

		assertNull(list);
	}
}
