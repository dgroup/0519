package jp.co.axiz.cooking.util.test;

import java.util.ArrayList;
import jp.co.axiz.cooking.util.CookingUtil;
import junit.framework.TestCase;

/**
 *
 * @author t.sugai
 *
 */
public class CookingUtilTest2 extends TestCase {
	public void testGetIngredients(){
		//『何をつくりますか？1:カレー　2:チャーハン』で使用するメソッドのテストです。
		//今回はチャーハンを選択したとします。
		int selectNumber = 2;

		//上記で初期化したselectNumberを引数としてgetIngredientsメソッドを呼び出します。
		//今回のテストではgetIngredientsメソッドの戻り値としてgetCharhangメソッド内の配列内情報を順番通りに全て取得します。
		ArrayList<String> list = CookingUtil.getIngredients(selectNumber);


		//処理結果と想定された処理結果を比較、確認します。
		//assertEqualsメソッドの第２引数と第３引数の値が一致すれば成功です。
		assertEquals("正しい結果ではない" ,"ごはん",list.get(0));		//getCharhang()[0]のデータ
		assertEquals("正しい結果ではない" ,"卵",list.get(1));			//getCharhang()[1]のデータ
		assertEquals("正しい結果ではない" ,"しょうゆ",list.get(2));		//getCharhang()[2]のデータ
		assertEquals("正しい結果ではない" ,"ごま油",list.get(3));		//getCharhang()[3]のデータ
		assertEquals("正しい結果ではない" ,"ハム",list.get(4));			//getCharhang()[4]のデータ
	}
}