/**
 *
 */
package sample;

import junit.framework.TestCase;

/**
 * @author kc
 *
 */
public class SampleTest extends TestCase {
	//テストメソッドの宣言
	public static void testChangeNum(){
		String beforeStr = "100";			//変換前の文字列
		int afterStrNum = 100;				//想定される処理結果の数値

		//オブジェクトの生成
		Sample ts = new Sample();
		//changeNumメソッドの呼び出し
		int resultInt = ts.changeNum(beforeStr);

		//処理結果と想定された処理結果を比較、確認
		assertEquals("数値に変換されていない",afterStrNum,resultInt);
	}
}
